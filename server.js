// index.js
// where your node app starts

// init project
var express = require('express');
var app = express();

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({ optionsSuccessStatus: 200 }));  // some legacy browsers choke on 204

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});


// your first API endpoint... 
app.get("/api/hello", function (req, res) {
  res.json({ greeting: 'hello API' });
});

app.get("/api/", function (req, res) {
  res.json({ "unix": new Date().getTime(), "utc": new Date().toUTCString() });
});

app.get("/api/:date", function (req, res) {
  const { date } = req.params;
  const datePattern = /^\d{4}[./-]\d{2}[./-]\d{2}$/;
  const specialCharacters = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

  if (date.match(datePattern)) {
    res.json({ "unix": new Date(date).getTime(), "utc": new Date(date).toUTCString() });
  } else if (!date.match(specialCharacters) && date.length <= 26) {
    res.json({ "unix": Number.parseInt(date), "utc": new Date(Number.parseInt(date)).toUTCString() })
  } else if (new Date(date).toString() != "Invalid Date") {
    res.json({ "unix": new Date(date).getTime(), "utc": new Date(date).toUTCString() });
  }

  res.json({ error: "Invalid Date" });

});

// listen for requests :)
var listener = app.listen(
  process.env.PORT,
  function () {
    console.log('Your app is listening on port ' + listener.address().port);
  });
